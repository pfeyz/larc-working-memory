======================
 Working Memory Pilot
======================

- for each experimental trial, record experimenter and participant
  input separately
- signify item selection (color change or something)
- show a happy pig at the completion of each child-input trial
- during training trials, replay jump back to demonstration trial when
  the child gets it wrong
- during experimental trials, accept and record incorrect input. show
  no indication of error.
- allow experimenter to repeat demonstration trials.

Notes
=====

- Need to record full input
- Feeding piggy the fruits after pointing to them provides valuable
  data. (this is lost in the current implementation.)

