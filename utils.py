import wx
import wx.lib.buttons
import glob
import random

class Hinter(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.iternum = wx.StaticText(self)
        self.current_user = wx.StaticText(self)
        self.hinttext = wx.StaticText(self)

        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.toygrid = wx.GridSizer(3,3)

        for i in range(9):
            self.toygrid.Add(wx.StaticText(self, label=str(i + 1)), -1, wx.EXPAND)

        self.sizer.Add(self.iternum, 1, wx.ALIGN_BOTTOM)
        self.sizer.Add(self.current_user, 1, wx.ALIGN_BOTTOM)
        self.sizer.Add(self.hinttext, 2, wx.ALIGN_BOTTOM)
        self.sizer.Add(self.toygrid, 1, wx.ALIGN_RIGHT)


        self.SetSizer(self.sizer)

    def clear_text(self):
        self.hinttext.SetLabel("")
        self.current_user.SetLabel("")
        self.iternum.SetLabel("")
        self.counter.SetLabel("")

    def set_fields(self, hint, number, current_user):
        self.set_hint(hint)
        self.set_num(number)
        self.set_user(current_user)


    def set_hint(self, hintlist):
        self.hinttext.SetLabel(" ".join([str(i + 1) for i in hintlist]))

    def set_user(self, current_user):
        self.current_user.SetLabel(current_user)

    def set_num(self, num):
        self.iternum.SetLabel(str(num))

    def set_counter(self):
        self.hinttext.SetLabel("")
        self.current_user.SetLabel("")

class RewardPanel(wx.Panel):
    """ Holds the image(s) to show the subject in between trials. """
    def __init__(self, parent, rewardimage=None):
        """
        Args
          `parent`: the parent frame
          `rewardimage`: a single image filename to be
          shown in between every trial.

        """

        wx.Panel.__init__(self, parent=parent)
        self.sizer = wx.GridSizer(1, 1)
        self.rewardimage = rewardimage
        self.sizer.Add(wx.StaticBitmap(self, bitmap=wx.Bitmap("images/happy-face.png")), 0, wx.ALIGN_CENTER)
        self.SetSizer(self.sizer)
        self.Layout()

class DataCollectorPanel(wx.Panel):
    """ Gets the info about the child and stuff"""

    def __init__(self, parent, metadata=None, redirect=False, filename=None):
        wx.Panel.__init__(self, parent)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.metadata = metadata
        self.done = wx.Button(self)
        self.done.SetLabel("Continue")
        self.done.SetName("start_button")
        self.fullscreen = wx.CheckBox(self, label="Fullscreen")
        self.fullscreen.SetValue(False)
        parent.ShowFullScreen(bool(self.fullscreen.GetValue()))
        self.fullscreen.Bind(wx.EVT_CHECKBOX,
                             lambda event: parent.ShowFullScreen(bool(self.fullscreen.GetValue())))

        self.expName = wx.TextCtrl(self)
        self.subjectID = wx.TextCtrl(self)

        name = wx.BoxSizer(wx.HORIZONTAL)
        name.Add(wx.StaticText(self, label="Experimenter Name"), 0, wx.ALL, 5)
        name.Add(self.expName, 1, wx.ALL | wx.EXPAND, 5)
        self.sizer.Add(name, 0, wx.ALL | wx.EXPAND)

        subID = wx.BoxSizer(wx.HORIZONTAL)
        subID.Add(wx.StaticText(self, label="Subject ID"), 0, wx.ALL, 5)
        subID.Add(self.subjectID, 1, wx.ALL | wx.EXPAND, 5)
        self.sizer.Add(subID, 0, wx.ALL | wx.EXPAND, 5)
        self.sizer.Add(self.fullscreen, 0)
        self.sizer.Add(self.done)

        self.SetSizer(self.sizer)
        self.Layout()

        self.done.Bind(wx.EVT_BUTTON, self.start)

    def start(self, event):
        self.metadata['expName'] = self.expName.GetLineText(0)
        self.metadata['subjectID'] = self.subjectID.GetLineText(0)
        self.metadata['fullscreen'] = self.fullscreen.GetValue()
        if self.metadata['expName'] in ["", None] or \
                self.metadata['subjectID'] in ["", None]:
            message = wx.MessageDialog(None, "You must fill out both fields", style=wx.OK)
            message.ShowModal()
        else:
            event.Skip()

class NamedButton(wx.lib.buttons.GenBitmapButton):
    def __init__(self, *args, **kwargs):
        filename = kwargs["imgfilename"]
        del kwargs["imgfilename"]
        wx.lib.buttons.GenBitmapButton.__init__(self, *args, **kwargs)
        self.filename = filename

class Fader(object):
    def __init__(self, button):
        self.button = button
        self.val = 0
        self._cancelled = False

    def cancel(self):
        self._cancelled = True

    def fade(self, target_color, time=1000):
        r, g, b = target_color
        if self._cancelled:
            self.button.SetBackgroundColour(wx.WHITE)
            self.button.Refresh()
            return
        if self.val >= 255:
            self.button.SetBackgroundColour(wx.WHITE)
            self.button.Refresh()
            return
        else:
            self.button.SetBackgroundColour(wx.Colour(self.val, self.val,
                                                 self.val, self.val))
            self.button.Refresh()
            self.val += 16
            wx.FutureCall(time / 25, lambda: self.fade(target_color, time))


class ImageGridPanel(wx.Panel):
    """ A grid of experimental simuli images.

    When one of the grids is clicked, an EVT_BUTTON event is
    fired. Currently the event is fired regardless of whether or not
    there is an image on that grid.

    TODO:
      - Should empty grid-touches be recorded?

    """

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.parent = parent
        self.buttons = []
        self.sizer = wx.GridSizer(3, 3)
        self.gridsize = 9
        self.backgroundcolor = (255, 255, 255)  # white

        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.visible_button_names = []
        self.button_filenames = {}
        self.load_buttons()
        self.fader = None
        # self.SetBackgroundColour(wx.Colour(*self.backgroundcolor))


    def load_buttons(self):
        """ Creates the bitmap buttons for the grid """

        images = self.get_images()
        screen_x, screen_y = wx.DisplaySize()
        ratio_x, ratio_y = screen_x / 1024.0, screen_y / 768.0
        scale = min(ratio_x, ratio_y, 1)
        for image in images:
            button = self.make_button(image, scale)
            self.buttons.append(button)
            self.button_filenames[button] = image
            button.Bind(wx.EVT_SET_FOCUS, lambda e: self.parent.keylogger.SetFocus())
            button.Hide()

    def populate_grid(self, mandatory, total=9):
        """ Puts `total` Bitmap Buttons on the ImageGridPanel.

        The indexis on the grid corresponding to the indexis in the
        `mandatory` list are guarenteed to have buttons. The indexis
        of the rest of the buttons (if any) needed to ensure `total`
        buttons are shown are picked at random. Blanks are generated
        by get_blank()

        You must call load_buttons before using this function

        """

        assert len(self.buttons)
        buttons = self.get_buttons(total)

        numextra = total - len(mandatory)
        empties = list(set(range(self.gridsize)) - set(mandatory))
        random.shuffle(empties)
        extras = empties[0:numextra]

        for index in range(self.gridsize):
            if index in mandatory or index in extras:
                button = buttons.pop()
                self.sizer.Add(button, -1, wx.EXPAND | wx.ALL, border=0)
                self.register(button, index)
                button.Show()
            else:
                self.sizer.Add(self.get_blank(), -1, wx.EXPAND | wx.ALL, border=0)
        self.Layout()

    def register(self, button, num):
        """ quick hack to get around my misunderstanding of python's
        issues with closures."""

        button.Bind(wx.EVT_BUTTON, lambda x: self.on_click(x, num))
        self.visible_button_names.append(self.button_filenames[button])

    def depopulate_grid(self):
        """ Removes from the sizer and hides all buttons on the grid."""

        for sizeritem in self.sizer.GetChildren():
            if sizeritem.IsWindow():
                button = sizeritem.GetWindow()
                self.sizer.Detach(button)
                button.Unbind(wx.EVT_BUTTON)
                button.Hide()
        self.sizer.Clear()
        self.visible_button_names = []

    def get_blank(self):
        return (0, 0)

    def get_buttons(self, num):
        random.shuffle(self.buttons)  # shuffles self.buttons in-place every time!
        return self.buttons[0:num]

    def get_images(self):
        return glob.glob("images/fruits/*.png")

    def make_button(self, image, scale=0.75):
        """ Creates a GenBitmapButton from image, scaled to `scale` percent"""

        button = NamedButton(parent=self, imgfilename=image, bitmap=wx.Bitmap(image))
        orig = wx.Image(image)
        h, w = orig.GetWidth(), orig.GetHeight()
        s = wx.BitmapFromImage(orig.Scale(h * scale, w * scale))
        button.SetBitmapSelected(s)
        button.SetBackgroundStyle(wx.BG_STYLE_COLOUR)
        button.SetBackgroundColour(wx.WHITE)
        return button

    def fade(self, button, target_color, time=1000, _val=100):
        """ fades a button from black to `target_color`.
        args:
          `button`: a wxButton (only been tested with GenBitmapButton)
          `fadecolor`: the color to fade/return to from black
          `time`: number of milliseconds fade should take (not sure
                  how accurate this is)

        _val is used internally to terminate recursion.
        """
        if self.fader:
            self.fader.cancel()
        self.fader = Fader(button)
        self.fader.fade(target_color, time)

    def on_click(self, event, num):
        obj = event.GetEventObject()
        # obj.SetFocus()
        self.fade(obj, obj.GetBackgroundColour())
        self.sizer.Layout()
        event.Int = num
        event.Skip()


class ImageGridTester(wx.App):
    def __init__(self, no):
        wx.App.__init__(self)
        self.frame = wx.Frame(None)
        self.grid = ImageGridPanel(self.frame)
        self.grid.populate_grid([4], 4)
        self.frame.Show()
        wx.FutureCall(500, self.do_later)

    def do_later(self):
        self.grid.depopulate_grid()
        self.grid.populate_grid([4], 4)
        self.grid.Layout()
        wx.FutureCall(500, self.do_later)

def test_image_grid():
    app = ImageGridTester(False)
    app.MainLoop()

if __name__ == "__main__":
    test_image_grid()
