""" Working Memory Experiment

The entire experiment is contained within a WMFrame. WMFrame contains
an ImageGridPanel, RewardPanel, and DataCollectorPanel.

ImageGridPanel contains the interface where the subject clicks on the
stimuli. RewardPanel displays an image in between
trials. DataCollectorPanel contains forms for metadata collection
before the experiment starts.

WMFrame maintains state and is responsible for switching between the
three panels.

TODO:
- make apple, pear and carrot bigger
- for each iteration, record which fruits were used

"""

from __future__ import print_function
import csv
import pprint
import wx
import wx.lib.buttons
import re
from collections import defaultdict
from os import path
from utils import (ImageGridPanel, RewardPanel, DataCollectorPanel, Hinter)

TRIGGER_KEY_CODES = [90, 47, 191]

class IterationData(object):
    """ Holds the data relevant to a single iteration

    Including the correct answer and the participant's response

    """

    def __init__(self, correct_sequence, total_pics_shown):
        """
        args:
          `correct_sequence`:
        """
        self.correct_sequence = correct_sequence
        self.num_shown = total_pics_shown
        self.observed_sequence = []

    def is_empty(self):
        return len(self.observed_sequence) == 0

    def observe(self, item):
        self.observed_sequence.append(item)

    def __str__(self):
        return "IterationData(%s -> %s)" % (self.correct_sequence, self.observed_sequence)

    def __repr__(self):
        return self.__str__()


class WMFrame(wx.Frame):
    """ The visual interface to the experiment """

    def __init__(self, all_trial_data):
        """ Sets everything up, basically.

        We're using two panels, one `fruits` for displaying the grid
        to be interacted with, and the other, `reward`, used as a
        break in between trials.

        """
        wx.Frame.__init__(self, None)

        # the screens we'll be switching between
        self.metadata = {}
        self.logfile = None
        self.metasizer = wx.BoxSizer(wx.VERTICAL)  # this is ridiculous and needs to be refactored (?)
        self.current_user = None  # whose turn is it to touch the touchscreen
        self.fruitscreen = ImageGridPanel(self)
        self.rewardscreen = RewardPanel(self)
        self.infoscreen = DataCollectorPanel(self, self.metadata)
        self.hinter = Hinter(self) # allows experimenter to see correct sequences

        # invisible and always on top to capture keystrokes
        self.keylogger = wx.Panel(self, -1, size=wx.Size(1, 1))

        # identifying which part of the experiment we're on
        self.trialsections = ["real", "practice"]  # is mutated. experiment
        self.cursection = self.trialsections.pop()   # ends when list is empty
        self.iteration = 0  # we start each trialsection at 0

        # recording data
        self.globaldata = defaultdict(list)  # trialsection-wide data
        self.all_trial_data = all_trial_data
        self.collecting_trial = None # where we'll store the observed buttonpresses etc

        # layout
        self.sizer = wx.BoxSizer()
        self.sizer.Add(self.fruitscreen, 1, wx.EXPAND)
        self.sizer.Add(self.rewardscreen, 1, wx.EXPAND)
        self.sizer.Add(self.infoscreen, 1, wx.EXPAND)

        self.metasizer.Add(self.sizer, 20, wx.EXPAND)
        self.metasizer.Add(self.hinter, 0, wx.EXPAND, wx.TOP)
        self.SetSizer(self.metasizer)

        self.fruitscreen.Bind(wx.EVT_BUTTON, self.clicked)
        self.infoscreen.Bind(wx.EVT_BUTTON, self.start)

        self.keylogger.SetFocus()
        self.rewardscreen.Hide()
        self.fruitscreen.Hide()
        self.infoscreen.Show()

        self.SetAutoLayout(True)
        self.Layout()

        self.timer = None  # used for rewardscreen

        self.datafile = "test-data.csv"
        self.prevdata = None

    def _is_spacebar(self, event):
        return hasattr(event, 'GetKeyCode') and event.GetKeyCode() == wx.WXK_SPACE

    def _is_startbutton(self, event):
        button = event.GetEventObject()
        try:
            return button.GetName() == "start_button"
        except ValueError:
            return False

    def section_finished(self, iteration):
        return iteration + 1 == len(self.all_trial_data[self.cursection])

    def got_key(self, event):
        """ Fired on every keypress. Reponsible for progression of experiment.

        SpaceBar -> Hide grid, show `reward` panel. Hold reward for 2
                    seconds. During this time, repopulate grid. Show
                    grid.

        """
        if self._is_spacebar(event) or self._is_startbutton(event):
            iteration = self.iteration
            if not self._is_startbutton(event):
                self.record_data(self.collecting_trial)
                if self.section_finished(iteration):
                    if len(self.trialsections) == 0:
                        print("Ending Experiment!")
                        # collect data here?
                        self.Close()
                        return
                    else:
                        self.cursection = self.trialsections.pop()
                        self.iteration = 0
                else:
                    self.globaldata[self.cursection].append(self.collecting_trial)
                    self.iteration += 1

            trialdata = IterationData(*self.all_trial_data[self.cursection][self.iteration])
            self.fruitscreen.Show() # needs to be shown to allow size recalulation
            self.fruitscreen.depopulate_grid()
            self.fruitscreen.populate_grid(trialdata.correct_sequence, trialdata.num_shown)

            self.show_reward()
            self.timer = wx.Timer(self)
            self.Bind(wx.EVT_TIMER, lambda event: self.show_stimuli())

            self.current_user = "E"
            self.hinter.set_fields(trialdata.correct_sequence, self.iteration + 1, self.current_user)
            self.Layout()
            self.timer.Start(2000, oneShot=True)

            self.collecting_trial = trialdata


        elif hasattr(event, 'GetKeyCode') and event.GetUniChar() in TRIGGER_KEY_CODES:
            self.record_data(self.collecting_trial)
            self.collecting_trial.observed_sequence = []
            self.current_user = 'P' if self.current_user == 'E' else 'E'
            self.hinter.set_user(self.current_user)
        elif hasattr(event, 'GetKeyCode'):
            print(event.GetUniChar())

    def image_basename(self, filename):
        """ Removes file path and extension. """
        fn, ext = path.splitext(path.basename(filename))
        return fn

    def record_data(self, data):
        correct = data.correct_sequence
        observed = data.observed_sequence
        datatowrite = "\t".join(
            [str(i) for i in
             [self.cursection,
              self.iteration + 1,
              "experimenter" if self.current_user == 'E' else \
                  "participant",
              ",".join([str(i + 1) for i in correct]),
              ",".join([str(i + 1) for i in observed]),
              int(correct == observed),
              ",".join([self.image_basename(fn) for
                        fn in self.fruitscreen.visible_button_names] ) + "\n"]])
        print(datatowrite)

        self.curdata.observed_sequence = []
        with open(self.logfile, 'a') as f:
            f.write(datatowrite)

    def start(self, event):
        filename = "subjectdata/working-memory-data-%s.tsv" % self.metadata['subjectID']
        self.logfile = filename
        headers = ["SECTION", "TRIAL", "WHO", "CORRECT-SEQUENCE",
                   "OBSERVED-SEQUENCE", "CORRECT?", "FRUITS"]
        with open(self.logfile, 'a') as f:
            f.write("\t".join(headers) + "\n")

        self.keylogger.Bind(wx.EVT_KEY_DOWN, self.got_key)  # gets all keys
        self.fruitscreen.Show()
        self.Layout()
        self.curdata = IterationData(*self.all_trial_data[self.cursection][self.iteration])
        self.fruitscreen.populate_grid(self.curdata.correct_sequence, self.curdata.num_shown)
        self.got_key(event)

    def show_reward(self):
        self.fruitscreen.Hide()
        self.infoscreen.Hide()
        self.rewardscreen.Show()

    def show_stimuli(self):
        self.fruitscreen.Show()
        self.rewardscreen.Hide()
        self.SetAutoLayout(True)
        self.Layout()

    def clicked(self, event):
        self.collecting_trial.observe(event.Int)
        self.keylogger.SetFocus()

class Trial(object):
    """ A singal working memory trial """
    def __init__(self, target_slots, total_shown):
        self.target_slots = target_slots
        self.total_shown = total_shown

if __name__ == "__main__":
    # the lists are organized as [[guaranteed locations], [total # of
    # locations]]
    trial_data = {"practice": [], "real": []}

    for trial in ("practice", "real"):
        with open("trialdata/%s.csv" % trial) as fh:
            for (num, line) in enumerate(csv.reader(fh)):
                if num == 0:  # skip headers
                    continue
                try:
                    target_slots = [int(x) - 1 for x in line[1:]]
                    total_slots = int(line[0])
                except ValueError:
                    message = ("Error on line {0} of {1}, should be a "
                               "comma-seperated value file of integers only\n"
                               "  {2}").format(num + 1, fh.name, ", ".join(line))
                    raise Exception(message)
                entry = [target_slots, total_slots]
                trial_data[trial].append(entry)

    app = wx.App(False)
    frame = WMFrame(trial_data)
    frame.Show()
    app.MainLoop()
