================
 Working Memory
================

TODO
----

- Add info on how to update trial data.

- Make sure item 3 in `Running Working Memory`_ is correct.

Overview
--------

This is a simple Memory Game to test the capacity of a child's working
memory. The children are shown a 3x3 grid with fruit on some of the
grid-boxes. The experimenter says "this and this and this" as they touch between
one and six of the fruit. The child then tries to touch the same fruit in the
same order.

Requirements
------------

You'll need `python 2.7`_ and `wxPython`_ installed in order to run the
experiment.

.. _`python 2.7`: http://python.org/download/releases/2.7.3/
.. _`wxPython`: http://wxpython.org/download.php

If you're running this on Windows make sure you're not using an Aero theme. It
seems to cause the fruit background fading to flicker. ``Windows 7 Basic`` theme
works for me.

You also might want to `remove the circles`_ that show up around the pointer
when using a touchscreen in Windows 7.

.. _`remove the circles`: http://www.volnaiskra.com/2010/10/how-to-remove-those-annoying-circles.html

Starting Working Memory
-----------------------

1. Run workingmemory.py. If you have python installed you might be able to just
   double click on the ``workingmemory`` file. If you're in a terminal, you can
   run

   ::

     python workingmemory.py

   where ``python`` runs python2.6 or later. To do this in windows make sure
   python is `on your path`_.

2. A dialog should pop up. Fill out the ``Experimenter Name`` and ``Subject ID``
   fields and check the "Fullscreen" box. Make sure you're not reusing a subject
   ID. The old data won't be lost but the data file for that subject id will
   contain multiple runs and will be ugly. Click continue and the first trial
   will begin.

Running Working Memory
----------------------

1. Look at the ``Correct Sequence`` hint and ``Model Grid`` helper (see `The
   Interface`_) in order to determine the sequence to present to the participant
   for this trial.

2. Use the touchscreen to press the sequence of fruits. Say "this" out loud each
   time you touch one. For example, for three fruits you'd say "I want *this*
   and *this* and *this*".

3. After you've done this you need to tell the program that it's not longer the
   experimenter that's touching the screen, but the participant. You do this by
   pressing the ``Z`` or ``/`` key; whichever one is easier. You should see the
   ``Whose Turn`` (see `The Interface`_) indicator change from ``E`` to
   ``P``. You can keep pressing it to toggle back and forth between the two.

4. You should now ask the child to repeat what you just did- touching the same
   fruits in the same order.

5. (optional) If the child is confused and you need to show them the sequence
   again, simply press the ``Z`` or ``/`` key again so that the ``Whose Turn``
   indicator shows ``P`` and go back to step 2.

6. Press spacebar to advance to the next trial. Go back to step 2 and repeat
   until the experiment ends.

7. When you're done, the data from the trials you just ran will be in a .tsv
   file called ``working-memory-SUBJECT_ID.tsv`` in the subjectdata folder.


.. _`on your path`: http://superuser.com/questions/143119/how-to-add-python-to-the-windows-path/143121

The Interface
-------------


.. figure:: screen.jpg

  The Working Memory experimental interface.

  The interface is organized like so ::

     _________________________________________________________   __
    |                   .                   .                 |    |
    |        1          .         2         .       3         |    |
    |                   .                   .                 |    |
    |.........................................................|    |
    |                   .                   .                 |    |
    |        4          .         5         .       6         |    |
    |                   .                   .                 |    |--- The Grid
    |.........................................................|    |    (for fruit)
    |                   .                   .                 |    |
    |        7          .         8         .       9         |    |
    |                   .                   .                 |    |
    |_________________________________________________________|  __|   __
    |                                                         |          |
    | Trial #  | Whose Turn | Correct Sequence  |  Model Grid |          |--- The Secret
    |_________________________________________________________|        __|    Display Panel


  Trial #
    The number of the trial you're currently on. This will restart when
    you finish the practice round and start the real one.

  Whose Turn
    Will read either ``E`` or ``P`` for experimenter or participant. Starts
    with ``P``. Is toggled by pressing the ``Z`` or ``/`` key.

  Correct Sequence
    A series of numbers (1-9) that represent the sequence of fruits the
    experimenter is to touch and the participant is to imitate. The numbers
    represent a spot on the grid. Use the ``Model Grid`` as a helper to figure
    out where on the board a number is.

  Model Grid
    A small grid of numbers showing the experimenter the numbering of
    the grid items. It makes reading the ``Correct Sequence`` easier.
